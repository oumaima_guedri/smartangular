import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevperComponent } from './devper.component';

describe('DevperComponent', () => {
  let component: DevperComponent;
  let fixture: ComponentFixture<DevperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DevperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
