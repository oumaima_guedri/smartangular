import { EditlivreComponent } from './editlivre/editlivre.component';
import { AddLivreComponent } from './add-livre/add-livre.component';
import { LivresComponent } from './livres/livres.component';
import { GererCompteComponent } from './gerer-compte/gerer-compte.component';
import { LoginComponent } from './login/login.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ScienceComponent } from './science/science.component';
import { InforinternetComponent } from './inforinternet/inforinternet.component';
import { LitteratureComponent } from './litterature/litterature.component';
import { DevperComponent } from './devper/devper.component';
import { CategorieComponent } from './categorie/categorie.component';
import { ContactComponent } from './contact/contact.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './page/home/home.component';
import { TableComponent } from './page/table/table.component';
import { ListelibraireComponent } from './listelibraire/listelibraire.component';
import { ListelecteurComponent } from './listelecteur/listelecteur.component';
import { AvisComponent } from './avis/avis.component';
import { EquipeComponent } from './equipe/equipe.component';


const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'table', component: TableComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  {path:'contact', component:ContactComponent},
  {path:'categorie', component:CategorieComponent},
  {path:'devper', component:DevperComponent},
  {path:'litterature', component:LitteratureComponent},
  {path:'infointernet', component:InforinternetComponent},
  {path:'science', component:ScienceComponent},
  {path:'inscription', component:InscriptionComponent},
  {path:'login', component:LoginComponent},
  {path:'gerer-compte', component:GererCompteComponent},
  {path:'livres', component:LivresComponent},
  {path:'addlivre', component:AddLivreComponent},
  {path:'LivresList', component:LivresComponent},
  {path:'editlivres', component:EditlivreComponent},
  {path:'update/:numSerie' , component:EditlivreComponent},
  {path: 'livresList', component:AddLivreComponent},
  {path:'listelibraire', component:ListelibraireComponent},
  {path:'listelecteur', component:ListelecteurComponent},
  {path: 'avis' , component:AvisComponent},
  {path: 'equipe' , component:EquipeComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
