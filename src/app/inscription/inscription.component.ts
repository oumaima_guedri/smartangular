import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../model/user';
import { InscritService } from '../services/inscrit.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {


  
  uri="http://localhost:8484/SmartBookStoreREST/rest/users/ajout/";

  
  Users: any
  msgErreur: any
  constructor(private http: HttpClient,private InscritService : InscritService) { }
  verifier (f:NgForm)
  {
  let cin = f.value["cin"]
  let nom = f.value["nom"];
  let prenom = f.value["prenom"];
  let email = f.value["email"];
  let login = f.value["login"];
  let  password = f.value["password"];
  let  adresse = f.value["adresse"];
  let telephone = f.value["telephone"];
  if (cin == 0 || nom == "" ||      prenom == "" || email == "" || login == "" ||password == "" ||adresse == "" || telephone == ""  )
  {
  this.msgErreur = "Merci de remplir tous les champs";
  }
  else{
    this.msgErreur =null;

    let user= new User(cin,nom,prenom,email,password,login,adresse,telephone);

        this.InscritService.persist(user)
        .subscribe((User)=> {
           this.Users = [User, ...this.Users];
         })
  alert("Utilisateur ajouter avec succès ");

    }

  }
   

  ngOnInit() {
  }

}
