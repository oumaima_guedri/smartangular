import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class InscritService {

  apiurl="http://localhost:8484/SmartBookStoreREST/rest/users/inscrit/";
  constructor(private http: HttpClient) { }
  persist(user: User){
    return this.http.post<User>(this.apiurl,user);
  }

}
