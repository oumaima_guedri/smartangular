import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './dashboard/sidebar/sidebar.component';
import { ToggleDirective } from './dashboard/sidebar/toggle.directive';
import { HomeComponent } from './page/home/home.component';
import { TableComponent } from './page/table/table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ContactComponent } from './contact/contact.component';
import { CategorieComponent } from './categorie/categorie.component';
import { DevperComponent } from './devper/devper.component';
import { LitteratureComponent } from './litterature/litterature.component';
import { InforinternetComponent } from './inforinternet/inforinternet.component';
import { ScienceComponent } from './science/science.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { LoginComponent } from './login/login.component';
import { GererCompteComponent } from './gerer-compte/gerer-compte.component';
import { HttpClientModule } from '@angular/common/http';
import { LivresComponent } from './livres/livres.component';
import { AddLivreComponent } from './add-livre/add-livre.component';
import { FormGroup, FormsModule } from '@angular/forms';
import { EditlivreComponent } from './editlivre/editlivre.component';
import { ListelecteurComponent } from './listelecteur/listelecteur.component';
import { ListelibraireComponent } from './listelibraire/listelibraire.component';
import { AddUtilisateurComponent } from './add-utilisateur/add-utilisateur.component';
import { AvisComponent } from './avis/avis.component';
import { EquipeComponent } from './equipe/equipe.component';

@NgModule({
  declarations: [

    AppComponent,
    SidebarComponent,
    ToggleDirective,
    HomeComponent,
    TableComponent,
    ContactComponent,
    CategorieComponent,
    DevperComponent,
    LitteratureComponent,
    InforinternetComponent,
    ScienceComponent,
    InscriptionComponent,
    LoginComponent,
    GererCompteComponent,
    LivresComponent,
    AddLivreComponent,
    EditlivreComponent,
    ListelecteurComponent,
    ListelibraireComponent,
    AddUtilisateurComponent,
    AvisComponent,
    EquipeComponent,


  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTooltipModule,
    MatRippleModule,
    HttpClientModule,
    FormsModule,


  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]

})
export class AppModule { }
